package lcd.exception;

public class InputException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8587366854396747545L;

	public InputException() {
		super("Value should be a positive integer");
	}
	
}
