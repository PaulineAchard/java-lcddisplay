package lcd;


import java.util.Scanner;

import lcd.exception.InputException;
import lcd.model.Display;
import lcd.service.ServiceDisplay;

public class Main {

	public static void main(String[] args) throws InputException {
		ServiceDisplay serviceDisplay = new ServiceDisplay(); 
		
		System.out.println("Enter a numeric value: ");
		Scanner scanner = new Scanner(System.in);
		
		Integer value;
		try {
			value = scanner.nextInt();
		} catch (Exception e) {
			throw new InputException();
		}finally {
			scanner.close();
		}
		
		Display display = serviceDisplay.getDisplay(value);
		System.out.println("Input value: " + display.getValue() );
		System.out.println("Output: ");
		System.out.println(display.getFirstLine());
		System.out.println(display.getSecondLine());
		System.out.println(display.getThirdLine());
	}

}
