package lcd.model;

public class Display {
	
	private int value;
	private String firstLine;
	private String secondLine;
	private String thirdLine;
	
	public Display(int value, String firstLine, String secondLine, String thirdLine) {
		this.value = value;
		this.firstLine = firstLine;
		this.secondLine = secondLine;
		this.thirdLine = thirdLine;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getFirstLine() {
		return firstLine;
	}
	
	public String getSecondLine() {
		return secondLine;
	}
	
	public String getThirdLine() {
		return thirdLine;
	}
	

}
