package lcd.model;

public class Digit {
	
	public int position;
	public int value;
	public String firstRow;
	public String secondRow;
	public String thirdRow;
	
	public Digit(int value) {
		super();
		this.value = value;
	}
	
	public String getFirstRow(){
		return firstRow;
	}
	
	public String getSecondRow() {
		return secondRow;
	}
	
	public String getThirdRow() {
		return thirdRow;
	}
	
	public void setFirstRow(String string) {
		this.firstRow = string;
	}
	
	public void setSecondRow(String string) {
		this.secondRow = string;
	}
	
	public void setThirdRow (String string) {
		this.thirdRow = string;
	}

	@Override
	public String toString() {
		return "Digit [value=" + value + ", firstRow=" + firstRow + ", secondRow=" + secondRow + ", thirdRow="
				+ thirdRow + "]";
	}
	
	
	
}
