package lcd.service;

import java.util.Arrays;

import lcd.model.Digit;

public class ServiceDigit {
	
	public Digit createNewDigit(int value) {
		Digit digit = new Digit(value);
		setFirstRow(digit);
		setSecondRow(digit);
		setThirdRow(digit);
		return digit;
	}

	private void setFirstRow(Digit digit) {
		if (Arrays.asList(0, 2, 3, 5, 6, 7, 8, 9).contains(digit.value)) {
			digit.setFirstRow(" - ");
		}else {
			digit.setFirstRow("   ");	
		}
	}
	
	private void setSecondRow(Digit digit) {
		if(digit.value == 0){
			digit.setSecondRow("| |");	
		}else if(Arrays.asList(1, 7).contains(digit.value)) {
			digit.setSecondRow("  |");	
		}else if(Arrays.asList(2, 3).contains(digit.value)) {
			digit.setSecondRow(" _|");	
		}else if(Arrays.asList(4, 8, 9).contains(digit.value)) {
			digit.setSecondRow("|_|");	
		}else if(Arrays.asList(5, 6).contains(digit.value)) {
			digit.setSecondRow("|_ ");	
		}else {
			digit.setSecondRow("   ");
		}
	}
	
	private void setThirdRow(Digit digit) {
		if(Arrays.asList(0, 6, 8).contains(digit.value)) {
			digit.setThirdRow("|_|");
		}else if(Arrays.asList(1, 4, 7, 9).contains(digit.value)) {
			digit.setThirdRow("  |");
		}else if(digit.value == 2) {
			digit.setThirdRow("|_ ");
		}else if(Arrays.asList(3, 5).contains(digit.value)) {
			digit.setThirdRow(" _|");
		}else{
			digit.setThirdRow("   ");	
		}
	}
	
}
