package lcd.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lcd.model.Digit;
import lcd.model.Display;

public class ServiceDisplay {
	
	ServiceDigit serviceDigit = new ServiceDigit();
	
	public Display getDisplay(int value) {
		List<Digit> digits = convertIntegerToListOfDigits(value).stream().map(integer -> serviceDigit.createNewDigit(integer)).collect(Collectors.toList());
		
		StringBuilder firstRow = new StringBuilder();
		StringBuilder secondRow = new StringBuilder();
		StringBuilder thirdRow = new StringBuilder();
		
		for (Digit digit : digits) {
			firstRow.append(digit.firstRow + " ");
			secondRow.append(digit.secondRow + " ");
			thirdRow.append(digit.thirdRow+ " ");
		}
		
		return new Display(value, firstRow.toString(), secondRow.toString(), thirdRow.toString());
	}
	
	private List<Integer> convertIntegerToListOfDigits(int value){
		List<Integer> result = new ArrayList<Integer>();
		char[] valueToString = String.valueOf(value).toCharArray();
		for (char c : valueToString) {
			result.add(new Integer(String.valueOf(c)));
		}
		return result;
	}
	
}
